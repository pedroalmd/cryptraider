
# Crypt Raider

●	Small UE5 dungeon puzzle game developed with C++ and Blueprint.

●	Project developed to have a better grasp on: Modular level design, Lumen’s lighting mechanism, characters blueprints, actor components, line tracing, sweeping, wake physics, overlap events and tags, UE5’s enhanced input and action mappings, debugging through drawing debug lines and spheres.

● This demo was developed as part of the Unreal Engine 5 Udemy Course I took.








# Screenshots

![App Screenshot](https://gitlab.com/pedroalmd/cryptraider/-/raw/main/Screenshots/Screenshot%202023-10-07%20182737.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/cryptraider/-/raw/main/Screenshots/Screenshot%202023-10-07%20182746.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/cryptraider/-/raw/main/Screenshots/Screenshot%202023-10-07%20182759.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/cryptraider/-/raw/main/Screenshots/Screenshot%202023-10-07%20182820.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/cryptraider/-/raw/main/Screenshots/Screenshot%202023-10-07%20183037.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/cryptraider/-/raw/main/Screenshots/Screenshot%202023-10-07%20183053.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/cryptraider/-/raw/main/Screenshots/Screenshot%202023-10-07%20183133.png?ref_type=heads)

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/pedroalmd/cryptraider/-/tree/main?ref_type=heads
```

Go to the project directory

```bash
  /CryptRaider
```

Run executable and enjoy the demo!

```bash
  CryptRaider.exe
```





## Objective

- Find the secret passage! ( Tip: try to grab an object and find out a wall you can palce it on!)
- Loot the statue and escape the crypt!

